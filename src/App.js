import React, { Component } from 'react';
import Calendar from "./components/Calendar"
import './App.css';

class App extends Component {
  constructor (props) {
    super(props)
    this.state = {}
  }
  
  render() {
    return (
      <div className="App">
        <header>
          <div id="logo">
            <span className="icon">date_range</span>
            <span>
              <b>Calendar</b>
            </span>
          </div>  
        </header>
        <main>
          <Calendar/>
        </main>
      </div>
    );
  }
}

export default App;
